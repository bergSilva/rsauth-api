package com.rssilva.rsauthapi.service.impl;

import com.rssilva.rsauthapi.model.Usuario;
import com.rssilva.rsauthapi.model.dto.UsuarioDTO;
import com.rssilva.rsauthapi.model.modelMapper.UsuarioMapper;
import com.rssilva.rsauthapi.repository.UsuarioRepository;
import com.rssilva.rsauthapi.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsuarioServiceImpl implements UsuarioService {


    private UsuarioRepository repository;
    private UsuarioMapper mapper;

    public UsuarioServiceImpl(UsuarioRepository repository) {
        this.repository = repository;
        this.mapper = new UsuarioMapper();
    }

    @Override
    public UsuarioDTO salvarUsuario(UsuarioDTO dto) {
        return mapper.toDTO(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public List<UsuarioDTO> listarTodos() {
        return this.repository.findAll().stream().map(this::toDto).collect(Collectors.toList());
    }

    @Override
    public UsuarioDTO buscarUsuario(UsuarioDTO dto) {
        return null;
    }

    @Override
    public void removerUsuario(Long idUsuario) {

    }

    @Override
    public UsuarioDTO toDto(Usuario usuario) {
        return mapper.toDTO(usuario);
    }
}
