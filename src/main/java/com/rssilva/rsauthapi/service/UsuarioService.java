package com.rssilva.rsauthapi.service;

import com.rssilva.rsauthapi.model.Usuario;
import com.rssilva.rsauthapi.model.dto.UsuarioDTO;

import java.util.List;

public interface UsuarioService {
    UsuarioDTO salvarUsuario(UsuarioDTO dto);

    List<UsuarioDTO> listarTodos();

    UsuarioDTO buscarUsuario(UsuarioDTO dto);

    void removerUsuario(Long idUsuario);

    <R> R toDto(Usuario usuario);
}
