package com.rssilva.rsauthapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsauthApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RsauthApiApplication.class, args);
	}

}
