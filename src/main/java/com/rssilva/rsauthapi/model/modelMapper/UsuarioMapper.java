package com.rssilva.rsauthapi.model.modelMapper;

import com.rssilva.rsauthapi.model.Usuario;
import com.rssilva.rsauthapi.model.dto.UsuarioDTO;

public class UsuarioMapper {

    public UsuarioDTO toDTO(Usuario e) {
        return UsuarioDTO.builder()
                .id(e.getId())
                .email(e.getEmail())
                .nome(e.getNome())
                .senha(e.getSenha())
                .build();
    }

   public Usuario toEntity(UsuarioDTO dto) {
        return Usuario.builder()
                .id(dto.getId())
                .email(dto.getEmail())
                .nome(dto.getNome())
                .senha(dto.getSenha())
                .build();
    }
}
