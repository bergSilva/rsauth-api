
package com.rssilva.rsauthapi.model.dto;

import lombok.*;

import javax.persistence.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO {
    private Long id;

    private String nome;

    private String email;

    private String senha;


}
